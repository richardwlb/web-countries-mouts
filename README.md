# Lista de países

<p>
  <img src="https://img.shields.io/badge/made%20by-RICHARD%20BREHMER-04D361?style=flat-square">
  <a href="https://opensource.org/licenses/MIT">
    <img alt="License" src="https://img.shields.io/badge/license-MIT-04D361?style=flat-square">
  </a>
</p>

Sobre Lista de países

Projeto realizado como teste para nova posição de React Developer.

https://richard-web-countries.netlify.app/

## Tecnologias

- ReactJS
- Apollo GraphQL
- react-router
- Substituição do redux pelo Local state management do Apollo Client
- @testing-library/react e Jest para testes
- CSS
- CI/CD Gitlab
- Publicação no Netlify


