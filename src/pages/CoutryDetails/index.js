import { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { client } from '../../services/Apollo';
import { areaVar, capitalVar, flagVar, nameVar, populationVar, topDomainVar } from '../../services/cache';
import Map from '../../components/Map';

import './styles.css';

export default function CountryDetails() {
  const { country } = useParams();
  const history = useHistory();
  
  const [name, setName] = useState("");
  const [capital, setCapital] = useState("");
  const [area, setArea] = useState("");
  const [population, setPopulation] = useState("");
  const [topDomain, setTopDomain] = useState("");

  useEffect( () => {
    // Busca valores do State Management do Apollo, caso não tenha dados salvos ja no browser.
    setName( !localStorage.getItem(`${country}_name`) ? nameVar() : localStorage.getItem(`${country}_name`));
    setCapital( !localStorage.getItem(`${country}_capital`) ? capitalVar() : localStorage.getItem(`${country}_capital`));
    setArea( !localStorage.getItem(`${country}_area`) ? areaVar() : localStorage.getItem(`${country}_area`));
    setPopulation( !localStorage.getItem(`${country}_population`) ? populationVar() : localStorage.getItem(`${country}_population`));
    setTopDomain(!localStorage.getItem(`${country}_topdomain`) ? topDomainVar() : localStorage.getItem(`${country}_topdomain`));
  }, []);

  function handleSaveData(e) {
    e.preventDefault();
    localStorage.setItem(`${name}_name`, name);
    localStorage.setItem(`${name}_capital`, capital);
    localStorage.setItem(`${name}_area`, area);
    localStorage.setItem(`${name}_population`, population);
    localStorage.setItem(`${name}_topdomain`, topDomain);

    toast("Os dados foram salvos no browser.");
  }

  if( !nameVar()) {
    history.push("/");
    return "";
  }

  return(
    <>
      <nav className="detail-navbar" >
          <button className="button-back" onClick={ () => history.goBack()} >
            Voltar
          </button>
      </nav>
        <ToastContainer
          position="top-center"
          autoClose={4000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
          <h4 className="country-details-title">
            {nameVar()}
          </h4>
          <div className="country-flag-container">
            <img 
              src={flagVar()} 
              alt="Flag"
              className="country-details-image"
            />
          </div>

          <div className="country-details-container">
            <div className="details-container" >
              <form onSubmit={handleSaveData} autoComplete="off">
                <div className="country-details-fields">
                  <label className="country-details-label" htmlFor="capital" >
                    Capital
                  </label>
                  <input 
                    id="capital"
                    className="field" 
                    value={capital} 
                    onChange={ e => setCapital(e.target.value) }
                  /> 
                </div>
                <div className="country-details-fields">
                  <label className="country-details-label" htmlFor="area" >
                    Área
                  </label>
                  <input 
                    id="area"
                    className="field" 
                    value={area} 
                    onChange={ e => setArea(e.target.value) }
                  /> 
                </div>
                <div className="country-details-fields">
                  <label className="country-details-label" htmlFor="population" >
                    População
                  </label>
                  <input 
                    id="population"
                    className="field" 
                    value={population} 
                    onChange={ e => setPopulation(e.target.value) }
                  /> 
                </div>
                <div className="country-details-fields">
                  <label className="country-details-label" htmlFor="top-domains" >
                    Top domínio
                  </label>
                  <input 
                    id="top-domains"
                    className="field" 
                    value={topDomain} 
                    onChange={ e => setTopDomain(e.target.value) }
                  /> 
                </div>
                <button type="submit">
                  Salvar
                </button>
              </form>
            </div>
            <ApolloProvider client={client}>
              <Map />
            </ApolloProvider >
          </div>
        </>
  );
}