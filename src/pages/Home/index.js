import { useState } from 'react';
import { ApolloProvider } from '@apollo/client';

import Countries from '../../components/Countries';
import { client } from '../../services/Apollo';

export default function Home() {
  const [countrySearch, setCountrySearch] = useState("");

  // Comment to test Branch.

  return(
    <div >
    <nav className="main-navbar" >
      <div className="filter-container">
        <input 
              className="filter" 
              value={countrySearch} 
              onChange={ e => setCountrySearch(e.target.value )}
              placeholder="Procure por um país"
          /> 
        </div>
      </nav>
      <ApolloProvider client={client}>
        <Countries countryName={countrySearch} />
      </ApolloProvider>
  </div >
  );
}