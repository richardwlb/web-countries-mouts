import CountryCard from '../CountryCard';

import './styles.css';

export default function ListCountries ( { countries }  ) {
  return(
    <div className="list-container">
      <div className="list-items">
        {countries.Country.map((country) => (
          <div key={country.name}>
            <CountryCard country={country} />
          </div>
          )
        )}
      </div>
    </div>
  )
}