import Loader from "react-loader-spinner";

import './styles.css'

export default function Loading() {
  
  return (
    <div className="loading-container " >
      <Loader
        type="Puff"
        color="#6491cc"
        height={100}
        width={100}
      />
    </div>
  )
}