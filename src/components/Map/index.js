import { useQuery, gql } from '@apollo/client'
import { MapContainer, TileLayer, Marker, Popup  } from 'react-leaflet'
import { locationVar, nameVar, distanceVar } from '../../services/cache';

import Loading from '../Loading';

import './styles.css';

export default function Map() {
  const initialPosition = {
    lat: locationVar().latitude,
    lng: locationVar().longitude
  }

  const COUNTRIES = gql`
  query GetCountries {
    Country (
      filter: {
        name_in: [
          "${distanceVar()[0].countryName}"
          , "${distanceVar()[1].countryName}"
          , "${distanceVar()[2].countryName}"
          , "${distanceVar()[3].countryName}"
          , "${distanceVar()[4].countryName}"
        ] 
      }
    ) {
      name
      location {
        latitude
        longitude
      }
    }
  }
  `;

  const { loading, error, data } = useQuery(COUNTRIES);

  if (loading) return (<Loading />)
  if (error) return `Error! ${error.message}`

  return(
    <div className="map-container">

        <MapContainer  
          style={{ height: "280px" }}
          center={initialPosition} 
          zoom={3} 
          key={initialPosition.lat}
          scrollWheelZoom={false}
        >
          <TileLayer 
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={initialPosition}>
              <Popup>
                {nameVar()}
              </Popup>
          </Marker>
          { 
            data.Country.map( ( value, index ) => {
              const countryName = value.name;
              const position = {
                lat: value.location.latitude,
                lng: value.location.longitude
              }

              const findDistance = distanceVar().find( ({ countryName }) => {
                  return countryName === value.name;
              });

              return(
                <Marker key={index} position={position}>
                    <Popup>
                      {` ${countryName} está a `}
                      <br />
                      { ` ${findDistance.distanceInKm} Km `}
                      <br />
                      { `de distancia de ${nameVar()}`}
                    </Popup>
                </Marker>
              );
            })
          }
        </MapContainer>
      5 Países mais próximos
    </div> 
  );
}