import { useHistory } from 'react-router-dom';
import { areaVar, capitalVar, flagVar, nameVar, populationVar, topDomainVar, distanceVar, locationVar } from '../../services/cache';

export default function CountryCard( { country }) {
  const history = useHistory();

  function loadCountryDetails(){
    //Local state management do Apollo Client;
    // Salva valores State Management do Apollo ao inves do Redux
    nameVar(country.name);
    flagVar(country.flag.svgFile);
    capitalVar(country.capital);
    areaVar(country.area);
    populationVar(country.population);
    topDomainVar(country.topLevelDomains[0].name);
    distanceVar(country.distanceToOtherCountries);
    locationVar(country.location);

    history.push(`/details/${country.name}`);
  }

  return(
      <div onClick={ () => { loadCountryDetails() } }
        className="country-card-container"
      >
        <h4 className="country-card-title">
          {country.name}
        </h4>
        <img 
          src={country.flag.svgFile} 
          alt=""
          className="country-card-image"
        />
        <div className="country-card-description">
          <h3>
            {/* {`Capital: ${country.capital}`} */}
            {!localStorage.getItem(`${country.name}_capital`) 
              ? `Capital: ${country.capital}` 
              : `Capital: ${localStorage.getItem(`${country.name}_capital`)}` }
          </h3>
        </div>
    </div>
  );
}
