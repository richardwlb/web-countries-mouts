import { useQuery, gql } from '@apollo/client'
import ListCountries from '../ListCountries'

import Loading from "../Loading";

export default function Countries({countryName}) {

  const COUNTRIES = gql`
    query GetCountries {
      Country ( 
        filter: { name_contains: "${countryName}"}
       ) {
        name
        capital
        area
        population
        flag {
          emoji
          emojiUnicode
          svgFile
        }
        location {
          latitude
          longitude
        }
        topLevelDomains {
          name
        }
        distanceToOtherCountries(
          first: 5
          orderBy: distanceInKm_asc
        ) {
          distanceInKm
          countryName
        }
      }
    }
  `;

  const { loading, error, data } = useQuery(COUNTRIES);

  if (loading) return (<Loading /> )
  if (error) return `Error! ${error.message}`

  return (
    <div >
      <ListCountries countries={data} />
    </div >
  );
}