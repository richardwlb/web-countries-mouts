import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom'
import Home from '../pages/Home';

import React from 'react';
 
describe('Home', () => {
  test('renders Home component', () => {
    const rendered = render(<Home />);
    const input = rendered.container.querySelector("input");
    expect(input.placeholder).toBe("Procure por um país");
    // screen.debug();
  });

});