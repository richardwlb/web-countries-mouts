import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom'
import ReactRouter from 'react-router';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom'

import CoutryDetails from '../pages/CoutryDetails';
import { areaVar, capitalVar, flagVar, nameVar, populationVar, topDomainVar, distanceVar, locationVar } from '../../src/services/cache';
 
describe('CoutryDetails', () => {
  test('renders CoutryDetails component', () => {
    
    nameVar("Brazil");
    flagVar("Bandeira");
    capitalVar("Brasilia");
    areaVar("999");
    populationVar("150");
    topDomainVar(".br");
    distanceVar(["Paraguay", "Bolivia", "Venezuela", "Guiana", "Guiana Francesa"]);
    locationVar("-20, -30");

    jest.spyOn(ReactRouter, 'useParams').mockReturnValue({ country: 'Brazil' });

    const history = createMemoryHistory()
      render(
      <Router history={history}>
        <CoutryDetails />
      </Router>
    )

    // screen.getByRole('');
    // screen.debug();

    expect(screen.getByText('Brazil')).toBeInTheDocument();
    expect(screen.getByDisplayValue('Brasilia')).toBeInTheDocument();
    expect(screen.getByAltText('Flag')).toBeInTheDocument();
    expect(screen.getByDisplayValue('999')).toBeInTheDocument();
    expect(screen.getByDisplayValue('150')).toBeInTheDocument();
    expect(screen.getByDisplayValue('.br')).toBeInTheDocument();

  });

});