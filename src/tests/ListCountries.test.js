import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom'
import ListCountries from '../components/ListCountries';

import React from 'react';
 
describe('ListCountries', () => {
  test('renders ListCountries component', () => {
    
    const countries = {
      Country: [
        {
          "area": 652230,
          "capital": "Kabul",
          "distanceToOtherCountries": (5) [
            {
              "countryName": "Pakistan",
              "distanceInKm": 580.1756134947343
            }, {
              "countryName": "Tajikistan",
              "distanceInKm": 858.6712938343873
            }, {
              "countryName": "Uzbekistan",
              "distanceInKm": 858.6712938343873
            }, {
              "countryName": "Turkmenistan",
              "distanceInKm": 858.6712938343873
            }, {
              "countryName": "Iran (Islamic Republic of)",
              "distanceInKm": 858.6712938343873
            }],
          "flag": {
            "svgFile": "https://restcountries.eu/data/afg.svg"
            },
          "location": { 
            "latitude": 33, 
            "longitude": 65
          },
          "name": "Afghanistan",
          "population": 27657145,
          "topLevelDomains": [{"name": ".af"}]
        }
      ]
    };
    
    render(<ListCountries countries={countries} />);
    // screen.debug();
    expect(screen.getByText('Afghanistan')).toBeInTheDocument();
    expect(screen.getByText('Capital: Kabul')).toBeInTheDocument();
    expect( screen.getByRole('img')).toBeInTheDocument();

  });

});