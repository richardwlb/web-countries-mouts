import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ApolloProvider } from '@apollo/client';

import Map from '../components/Map';
import { locationVar, nameVar, distanceVar } from '../../src/services/cache';
import { client } from '../../src/services/Apollo';


import React from 'react';
 
describe('Map', () => {
  test('renders Map component', async () => {
    
    nameVar("Brazil");
    locationVar("-34, -64");
    distanceVar(["United States of America", "Colombia", "Trinidad and Tobago", "Mexico", "Guyana"]);
    
    const rendered = render (
      <ApolloProvider client={client}>
        <Map />
      </ApolloProvider>
      );

    const div = rendered.container.querySelector("div");
    expect(div.className).toBe("loading-container ");
    // screen.debug();

  });

});