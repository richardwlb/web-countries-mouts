import { InMemoryCache, makeVar } from '@apollo/client';

export const nameVar = makeVar("");
export const flagVar = makeVar("");
export const capitalVar = makeVar("");
export const areaVar = makeVar("");
export const populationVar = makeVar("");
export const topDomainVar = makeVar("");
export const distanceVar = makeVar("");
export const locationVar = makeVar("");

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        name: {
          read() {
            return nameVar();
          },
        },
        flag: {
          read() {
            return flagVar();
          },
        },
        capital: {
          read() {
            return capitalVar();
          },
        },
        area: {
          read() {
            return areaVar();
          },
        },
        population: {
          read() {
            return populationVar();
          },
        },
        topdomain: {
          read() {
            return topDomainVar();
          },
        },
        distance: {
          read() {
            return distanceVar();
          },
        },
        location: {
          read() {
            return locationVar();
          },
        },
      },
    },
  },
})