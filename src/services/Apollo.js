import { ApolloClient } from '@apollo/client';
import { cache } from './cache';

export const client = new ApolloClient({
  uri: 'https://countries-274616.ew.r.appspot.com',
  cache
});