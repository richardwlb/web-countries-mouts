import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';

import { client } from './services/Apollo';

import Home from './pages/Home';
import CountryDetails from './pages/CoutryDetails';

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <ApolloProvider client={client}> 
          <Route component={Home} path="/" exact />
          <Route component={CountryDetails} path="/details/:country" />
        </ApolloProvider >
      </Switch>
    </BrowserRouter>
  )
}

export default Routes;